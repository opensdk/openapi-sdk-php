<?php

// 开放服务环境域名
use Mingyuanyun\Core\SDK;
use Mingyuanyun\Printing\V20230207\TemplateQueryClient;
use Mingyuanyun\Printing\V20230207\TemplateQueryRequest;

$host = 'https://xxx.domain.com';
// 凭证 Secret ID
$secretId = 'secret_id_123';
// 凭证 Secret Key
$secretKey = 'secret_key_123';

SDK::loadConfig([
    'secretId'  => $secretId,
    'secretKey' => $secretKey,
    'server'    => [
        'host' => $host,
    ],
]);

$credentials = SDK::getDefaultCredentials();
$request     = new TemplateQueryRequest();
//如果有业务场景过滤需要，则加上参数
$request->setBizSceneCode("abc");

$client = new TemplateQueryClient(SDK::getDefaultCredentials(), $request);

$response = $client->invoke();

if (!$response->isSuccess(true)) {
    // 失败处理
} else {
    $templateList = $response->getTemplateData();
}
