<?php

use Mingyuanyun\Core\SDK;
use Mingyuanyun\Message\V20230417\ChannelPushClient;
use Mingyuanyun\Message\V20230417\ChannelPushRequest;
use Mingyuanyun\Message\V20230417\ScenePushClient;
use Mingyuanyun\Message\V20230417\ScenePushRequest;
use Mingyuanyun\Message\V20230417\Support\ChannelType;
use Mingyuanyun\Message\V20230417\Support\Receiver;
use Mingyuanyun\Message\V20230417\Support\SceneMessage;

$host = 'https://xxx.domain.com';
// 凭证 Secret ID
$secretId = 'secret_id_123';
// 凭证 Secret Key
$secretKey = 'secret_key_123';

SDK::loadConfig([
    'secretId'  => $secretId,
    'secretKey' => $secretKey,
    'server'    => [
        'host' => $host,
    ],
]);

/**
 * 推送渠道消息示例
 */

// 准备请求所需的数据
$request = new ChannelPushRequest();
$request->setChannelType(ChannelType::APP)
    ->setSource('sdk.message.test')
    ->setTenantCode('tenantest')
    ->setMessageText('消息标题', '消息内容详情')
    ->setReceivers([
        new Receiver('app_user_123', '测试 APP', 'ios'),
        new Receiver('app_user_456', '测试 APP', 'oppo'),
    ]);

// 实例化对应的渠道消息推送接口客户端
$client = new ChannelPushClient($this->credentials, $request);

// 发起推送请求
$response = $client->invoke();

if ($response->isSuccess(true)) {
    $taskId = $response->getTaskId();
}

/**
 * 推送场景消息示例
 */

// 准备请求所需的数据
$message = new SceneMessage(['title' => '消息标题', 'companyName' => '公司名称', 'projectName' => '项目名称']);
$request = new ScenePushRequest();
$request->setSceneCode('scene.test')
    ->setSource('sdk.message.test')
    ->setTenantCode('tenantest')
    ->setMessage($message)
    ->setReceivers([
        new Receiver('app_user_123'),
        new Receiver('app_user_456'),
    ]);

// 实例化对应的渠道消息推送接口客户端
$client = new ScenePushClient($this->credentials, $request);

// 发起推送请求
$response = $client->invoke();

if ($response->isSuccess(true)) {
    $taskId = $response->getTaskId();
}