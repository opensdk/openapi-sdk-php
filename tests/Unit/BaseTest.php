<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\BaseClient;
use Mingyuanyun\BaseRequest;
use Mingyuanyun\Core\Credentials\Credentials;
use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\HttpSender;
use Mingyuanyun\Core\SDK;
use Mingyuanyun\Core\Support\RequestOptions;
use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase
{
    /**
     * @var array
     */
    protected $config;

    protected function setUp(): void
    {
        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'server' => [
                'host' => 'https://www.server.com',
            ],
        ];

        SDK::loadConfig($this->config);
    }

    protected function tearDown(): void
    {
        SDK::clearConfig();
        HttpSender::cancelMock();
    }

    // 应该可以自动加载访问凭证并成功请求接口
    public function test_should_invoke_success_with_autoload_access_token()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'access_token' => 'token_123',
            'expires_in'   => 3600,
        ]);
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code' => 0,
            'message' => 'with token success.',
        ]);

        $client = new WithTokenClient(SDK::getDefaultCredentials(), new WithTokenRequest);
        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals('with token success.', $response->getBody('message'));
    }

    // 如果提供了访问凭证，应该可以成功请求接口
    public function test_should_invoke_success_if_access_token_provided()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code' => 0,
            'message' => 'with token success.',
        ]);
        
        $credentials = Credentials::client(SDK::config()->secretId, SDK::config()->secretKey, 'token_123');

        $client = new WithTokenClient($credentials, new WithTokenRequest);
        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals('with token success.', $response->getBody('message'));
    }

    // 应该可以不加载访问凭证并成功请求接口
    public function test_should_invoke_success_without_access_token()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code' => 0,
            'message' => 'with token success.',
        ]);

        $client = new WithoutTokenClient(SDK::getDefaultCredentials(), new WithoutTokenRequest);
        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals('with token success.', $response->getBody('message'));
    }

    // 加载访问凭证失败时，应该要抛出相关的异常信息
    public function test_should_throw_exception_if_prepare_access_token_faild()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'Result'  => false,
            'Message' => 'token faild.',
        ]);

        $this->expectException(RequestException::class);

        $client = new WithTokenClient(SDK::getDefaultCredentials(), new WithTokenRequest);
        $client->invoke();
    }
}

class WithTokenClient extends BaseClient
{
    protected function getUri()
    {
        return '/';
    }

    protected function getMethod()
    {
        return RequestOptions::METHOD_GET;
    }

    protected function resolveRequestValues()
    {
        return;
    }
}

class WithTokenRequest extends BaseRequest {}

class WithoutTokenClient extends BaseClient
{
    protected $isRequiredAccessToken = false;
    
    protected function getUri()
    {
        return '/';
    }

    protected function getMethod()
    {
        return RequestOptions::METHOD_GET;
    }

    protected function resolveRequestValues()
    {
        return;
    }
}

class WithoutTokenRequest extends BaseRequest {}
