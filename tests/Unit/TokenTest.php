<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\HttpSender;
use Mingyuanyun\Core\SDK;
use Mingyuanyun\Token\V20230109\AccessTokenClient;
use Mingyuanyun\Token\V20230109\AccessTokenRequest;
use PHPUnit\Framework\TestCase;

/**
 * 访问令牌测试类
 */
class AccessTokenTest extends TestCase
{
    /**
     * @var array
     */
    protected $config;

    protected function setUp(): void
    {
        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'server' => [
                'host' => 'https://www.server.com',
            ],
        ];

        SDK::loadConfig($this->config);
    }

    protected function tearDown(): void
    {
        SDK::clearConfig();
        HttpSender::cancelMock();
    }

    // 获取访问令牌数据
    public function test_should_get_access_token()
    {
        HttpSender::mockResponse(200, [], [
            'access_token' => 'token_123',
            'expires_in'   => 7200,
            'token_type'   => 'Bearer',
        ]);
        
        $client = new AccessTokenClient(SDK::getDefaultCredentials(), new AccessTokenRequest);

        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals('token_123', $response->getAccessToken());
        $this->assertEquals(7200, $response->getExpireTime());

        $this->assertEquals('accessToken: (7200) token_123', $response->toString());
    }

    // 获取访问令牌失败需要能得到相关的错误信息
    public function test_should_get_faild_info()
    {
        HttpSender::mockResponse(503, [], 'Faild, invalid secret key.');

        $client = new AccessTokenClient(SDK::getDefaultCredentials(), new AccessTokenRequest);

        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertFalse($response->isSuccess());
        $this->assertEmpty($response->getAccessToken());
        $this->assertStringContainsString('invalid secret key', $response->getErrMsg());
    }
}