<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\Exception\InvalidArgumentException;
use Mingyuanyun\Core\HttpSender;
use Mingyuanyun\Core\SDK;
use Mingyuanyun\Printing\V20230207\TemplateQueryClient;
use Mingyuanyun\Printing\V20230207\TemplateQueryRequest;
use PHPUnit\Framework\TestCase;

/**
 * 套打测试类
 */
class PrintingTest extends TestCase
{
    /**
     * @var array
     */
    protected $config;

    protected $credentials;

    protected function setUp():void
    {
        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'server' => [
                'host' => 'https://www.server.com',
            ],
        ];

        SDK::loadConfig($this->config);
        $this->credentials = SDK::getDefaultCredentials();
        $this->credentials->setToken('test_access_token');
    }

    protected function tearDown():void
    {
        SDK::clearConfig();
        HttpSender::cancelMock();
    }

    /**
     * 请求套打查询模板应该获取到模板列表
     */
    public function test_should_query_print_template()
    {
        HttpSender::mockResponse(200, [], [
            'code' => 0,
            'msg' => '成功',
            'data'    => [
                [
                    'templateId' => '123',
                    'templateName' => '第一个模板',
                ],
                [
                    'templateId' => '456',
                    'templateName' => '第二个模板',
                ]
            ],
        ]);

        $client = new TemplateQueryClient($this->credentials, new TemplateQueryRequest());
        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertNotEmpty($response->getTemplateList());
        $this->assertCount(2, $response->getTemplateList());
        $this->assertEquals([
            [
                'templateId' => '123',
                'templateName' => '第一个模板',
            ],
            [
                'templateId' => '456',
                'templateName' => '第二个模板',
            ]
        ], $response->getTemplateList());
    }

    /**
     * 请求套打查询模板失败，获取到失败信息
     */
    public function test_should_query_print_template_faild_info()
    {
        HttpSender::mockResponse(200, [], [
            'code' => 15003,
            'msg' => '业务场景不存在',
            'data' => null,
        ]);

        $client = new TemplateQueryClient($this->credentials, new TemplateQueryRequest());

        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertEquals([], $response->getTemplateList());
        $this->assertEquals(15003, $response->getErrCode());
        $this->assertEquals('业务场景不存在', $response->getErrMsg());
    }

    /**
     * 如果提供了类型错误的bizSceneCode参数，抛出异常
     */
    public function test_should_throw_exception_if_got_invalid_biz_scene_code()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/must be a string/');

        $request = new TemplateQueryRequest();
        $request->setBizSceneCode([]);
        new TemplateQueryClient($this->credentials, $request);
    }

    /**
     * 如果提供了空的bizSceneCode参数，抛出异常
     */
    public function test_should_throw_exception_if_got_empty_biz_scene_code()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/cannot be empty/');

        $request = new TemplateQueryRequest();
        $request->setBizSceneCode('');
        new TemplateQueryClient($this->credentials, $request);
    }
}
