<?php

use Mingyuanyun\Core\Exception\ClientException;
use Mingyuanyun\Core\HttpSender;
use Mingyuanyun\Core\SDK;
use Mingyuanyun\Message\V20230417\ChannelPushClient;
use Mingyuanyun\Message\V20230417\ChannelPushRequest;
use Mingyuanyun\Message\V20230417\ScenePushClient;
use Mingyuanyun\Message\V20230417\ScenePushRequest;
use Mingyuanyun\Message\V20230417\Support\ChannelType;
use Mingyuanyun\Message\V20230417\Support\Receiver;
use Mingyuanyun\Message\V20230417\Support\SceneMessage;
use PHPUnit\Framework\TestCase;

/**
 * 消息服务测试类
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 */
class MessageTest extends TestCase
{
    /**
     * @var array
     */
    protected $config;

    protected $credentials;

    protected function setUp(): void
    {
        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'server'    => [
                'host' => 'https://www.server.com',
            ],
        ];

        SDK::loadConfig($this->config);
        $this->credentials = SDK::getDefaultCredentials();
        $this->credentials->setToken('test_access_token');
    }

    protected function tearDown(): void
    {
        SDK::clearConfig();
        HttpSender::cancelMock();
    }

    /**
     * 传参正常，渠道消息推送成功
     */
    public function test_should_channel_push_success_if_args_is_right()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        $receiver = new Receiver('测试 APP', 'app_123', 'ios');

        $request = new ChannelPushRequest();
        $request->setChannelType(ChannelType::APP)
            ->setSource('sdk.message.test')
            ->setTenantCode('tenantest')
            ->setMessageText('测试消息标题', '测试消息内容详情', 'http://message/show')
            ->setReceivers([$receiver]);

        $client = new ChannelPushClient($this->credentials, $request);

        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(123, $response->getTaskId());
    }

    /**
     * 如果没有提供推送所需的基础数据，渠道消息推送失败
     */
    public function test_should_channel_push_failded_if_not_provide_base_args()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        $receiver = new Receiver('测试 APP', 'app_123', 'ios');

        // 没有提供来源标识以及渠道类型的数据
        $request = new ChannelPushRequest();
        $request->setTenantCode('tenantest')
            ->setMessageText('测试消息标题', '测试消息内容详情')
            ->setReceivers([$receiver]);

        $this->expectException(ClientException::class);

        new ChannelPushClient($this->credentials, $request);
    }

    /**
     * 如果没有提供消息内容数据，渠道消息推送失败
     */
    public function test_should_channel_push_failded_if_not_provide_message_data()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        $receiver = new Receiver('测试 APP', 'app_123', 'ios');

        // 没有提供消息内容的数据
        $request = new ChannelPushRequest();
        $request->setChannelType(ChannelType::WX_OFFICIAL_ACCOUNT)
            ->setSource('sdk.message.test')
            ->setTenantCode('tenantest')
            ->setReceivers([$receiver]);

        $this->expectException(ClientException::class);

        new ChannelPushClient($this->credentials, $request);
    }

    /**
     * 如果没有提供接收人数据，渠道消息推送失败
     */
    public function test_should_channel_push_failded_if_not_provide_receivers_data()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        // 没有提供接收人的数据
        $request = new ChannelPushRequest();
        $request->setChannelType(ChannelType::THIRD_SYSTEM)
            ->setSource('sdk.message.test')
            ->setTenantCode('tenantest')
            ->setMessageText('测试消息标题', '测试消息内容详情');

        $this->expectException(ClientException::class);

        new ChannelPushClient($this->credentials, $request);
    }

    /**
     * 传参正常，场景消息推送成功
     */
    public function test_should_scene_push_success_if_args_is_right()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        $receiver = new Receiver('测试 APP', 'app_123', 'ios');

        $request = new ScenePushRequest();
        $request->setSceneCode('scene.test')
            ->setSource('sdk.message.test')
            ->setTenantCode('tenantest')
            ->setMessage(new SceneMessage(['title' => '测试消息标题', 'content' => '测试消息内容详情']))
            ->setReceivers([$receiver]);

        $client = new ScenePushClient($this->credentials, $request);

        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(123, $response->getTaskId());
    }

    /**
     * 如果没有提供推送所需的基础数据，场景消息推送失败
     */
    public function test_should_scene_push_failded_if_not_provide_base_args()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        $receiver = new Receiver('测试 APP', 'app_123', 'ios');

        // 没有提供 来源标识 以及 场景编码 的数据
        $request = new ScenePushRequest();
        $request->setTenantCode('tenantest')
            ->setMessage(new SceneMessage(['title' => '测试消息标题', 'content' => '测试消息内容详情']))
            ->setReceivers([$receiver]);

        $this->expectException(ClientException::class);

        new ScenePushClient($this->credentials, $request);
    }

    /**
     * 如果没有提供消息内容数据，场景消息推送失败
     */
    public function test_should_scene_push_failded_if_not_provide_message_data()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        $receiver = new Receiver('测试 APP', 'app_123', 'ios');

        // 没有提供消息内容的数据
        $request = new ScenePushRequest();
        $request->setSceneCode('scene.test')
            ->setSource('sdk.message.test')
            ->setTenantCode('tenantest')
            ->setReceivers([$receiver]);

        $this->expectException(ClientException::class);

        new ScenePushClient($this->credentials, $request);
    }

    /**
     * 如果没有提供接收人数据，渠道消息推送失败
     */
    public function test_should_scene_push_failded_if_not_provide_receivers_data()
    {
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], [
            'code'    => 0,
            'message' => 'success',
            'data'    => [
                'taskId' => 123,
            ],
        ]);

        // 没有提供接收人的数据
        $request = new ScenePushRequest();
        $request->setSceneCode('scene.test')
            ->setSource('sdk.message.test')
            ->setTenantCode('tenantest')
            ->setMessage(new SceneMessage(['title' => '测试消息标题', 'content' => '测试消息内容详情']));

        $this->expectException(ClientException::class);

        new ScenePushClient($this->credentials, $request);
    }
}
