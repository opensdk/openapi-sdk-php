# Mingyuanyun OpenAPI SDK for PHP

[![Latest Stable Version](https://poser.pugx.org/mingyuanyun/openapi-sdk-php/v/stable)](https://packagist.org/packages/mingyuanyun/openapi-sdk-php)
[![Total Downloads](https://poser.pugx.org/mingyuanyun/openapi-sdk-php/downloads)](https://packagist.org/packages/mingyuanyun/openapi-sdk-php)
[![License](https://poser.pugx.org/mingyuanyun/openapi-sdk-php/license)](https://packagist.org/packages/mingyuanyun/openapi-sdk-php)


OpenAPI SDK PHP 是基于 PHP 语言帮助开发者快速进行开放服务产品能力访问的便捷开发包。由 [Mingyuanyun OpenAPI SDK Core for PHP](https://opengit.mysre.cn/opensdk/openapi-sdk-php) 提供底层支持。

---  

## 发行说明

目前无具体产品服务接入，处于稳定性内测阶段。

TODO:  

1. 套打服务除「查询模板」以外更多能力接入

---  

## 安装依赖

Mingyuanyun OpenAPI SDK for PHP 的安装需要依赖到[全局安装 Composer](https://getcomposer.org/doc/00-intro.md#globally)，如果你的系统已经准备就绪，可以在项目目录下运行以下命令来拉取依赖：

```
composer require mingyuanyun/openapi-sdk-php
```

> 如果你的 Composer 版本是 2.0 以上，为了避免本地环境与运行环境 PHP 版本不一致，可以加上 --ignore-platform-reqs 参数来避免本地 PHP 版本约束

---  

## 快速使用

在您正式开始使用该开发包前，请先咨询获取您系统的专属凭证。  

```php
<?php

use Mingyuanyun\Token\V20230109\AccessTokenClient;
use Mingyuanyun\Core\SDK;

// 开放服务环境域名
$host = 'https://xxx.domain.com';
// 凭证 Secret ID
$secretId = 'secret_id_123';
// 凭证 Secret Key
$secretKey = 'secret_key_123';

SDK::loadConfig([
    'secretId'  => $secretId,
    'secretKey' => $secretKey,
    'server'    => [
        'host' => $host;
    ],
]);

$credentials = SDK::getDefaultCredentials();
$request     = new AccessTokenRequest();

$client = new AccessTokenClient($credentials, $request);

$response = $client->invoke();

if (!$response->isSuccess(true)) {
    // 失败处理
} else {
    $accessToken = $response->getAccessToken();
}
```

---  

### 发布日志

请查看[发行说明](/CHANGELOG.md)。