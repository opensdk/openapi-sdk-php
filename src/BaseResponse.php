<?php

namespace Mingyuanyun;

use Mingyuanyun\Core\Exception\ResponseException;
use Mingyuanyun\Core\Response;
use Mingyuanyun\Core\ResponseInterface;
use Mingyuanyun\Core\Support\Traits\AccessTrait;

/**
 * 客户端服务响应数据抽象基类
 *
 * @method Response getResponse() 获取原始的响应数据类
 *
 * @codeCoverageIgnore
 */
abstract class BaseResponse implements ResponseInterface
{
    use AccessTrait;

    /**
     * 这里是一段装饰器模式的使用实现
     * 为了让不同服务接口的 ResponseInterface 实现类更友好地给调用方
     * 提供数据访问、以及逻辑判断能力
     */

    /**
     * @var Response
     */
    protected $response;

    /**
     * 响应业务错误码，通常无错误为 0
     *
     * @var int
     */
    public $errcode = self::ERRCODE_OK;

    /**
     * 响应业务错误描述，只有当业务错误码不为成功值时才有内容
     *
     * @var string
     */
    public $errmsg = '';

    /**
     * 构造器
     */
    public function __construct(Response $response)
    {
        if (!$response) {
            throw new ResponseException('Invalid response.');
        }
        $this->response = $response;

        $this->unmarshal();
    }

    /**
     * 解析响应数据
     */
    abstract protected function unmarshal();

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }

    /**
     * @return mixed
     */
    public function getBody($key, $default = null)
    {
        return $this->response->getBody($key, $default);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->response->getHeaders();
    }

    /**
     * @return bool
     */
    public function isSuccess($soEasy = false, $okCode = self::ERRCODE_OK)
    {
        return $this->response->isSuccess($soEasy, $okCode);
    }

    /**
     * @return bool
     */
    public function isJson()
    {
        return $this->response->isJson();
    }

    /**
     * @return void
     */
    public function setRetryInfo($snapshots, $clientRetryTiems, $serverRetryTimes)
    {
        $this->response->setRetryInfo($snapshots, $clientRetryTiems, $serverRetryTimes);
    }

    /**
     * @return int
     */
    public function getClientRetryTimes()
    {
        return $this->response->getClientRetryTimes();
    }

    /**
     * @return int
     */
    public function getServerRetryTimes()
    {
        return $this->response->getServerRetryTimes();
    }

    /**
     * @return BaseRequest
     */
    public function getRequest()
    {
        return $this->response->getRequest();
    }

    /**
     * Error Code
     *
     * @return int
     */
    public function getErrCode()
    {
        return $this->errcode;
    }

    /**
     * Error Mesage
     *
     * @return string
     */
    public function getErrMsg()
    {
        return $this->errmsg;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->response->toString();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->response->toArray();
    }

    /**
     * 魔术方法，将类转成字符串
     *
     * @codeCoverageIgnore
     */
    public function __toString()
    {
        return $this->toString();
    }
}
