<?php

namespace Mingyuanyun\Printing\V20230207;

use Mingyuanyun\BaseClient;
use Mingyuanyun\Core\Credentials\ClientCredentials;
use Mingyuanyun\Core\Support\RequestOptions;

/**
 * 查询模板
 */
class TemplateQueryClient extends BaseClient
{
    /**
     * @var ClientCredentials
     */
    protected $credentials;

    /**
     * 服务版本
     *
     * @var string
     */
    protected $version = '2023-02-07';

    /**
     * 客户端请求数据类
     *
     * @var TemplateQueryRequest
     */
    protected $request;

    /**
     * @return string
     */
    protected function getUri()
    {
        return '/print/template/query';
    }

    /**
     * @return string
     */
    protected function getMethod()
    {
        return RequestOptions::METHOD_GET;
    }

    /**
     * 查询模板服务构造器
     *
     * @param ClientCredentials     $credentials    使用开放服务所需要的凭证数据
     * @param TemplateQueryRequest  $request        请求服务所提供的请求数据
     */
    public function __construct(ClientCredentials $credentials, TemplateQueryRequest $request)
    {
        parent::__construct($credentials, $request);
    }

    /**
     * @return void
     */
    protected function resolveRequestValues()
    {
        if (!empty($this->request->getBizSceneCode())) {
            $this->request->setQuery(['bizSceneCode' => $this->request->getBizSceneCode()]);
        }
    }

    /**
     * 发起请求，返回结果
     * @return TemplateQueryResponse
     */
    public function invoke()
    {
        return new TemplateQueryResponse(parent::invoke());
    }
}
