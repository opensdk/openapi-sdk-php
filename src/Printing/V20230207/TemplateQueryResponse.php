<?php

namespace Mingyuanyun\Printing\V20230207;

use Mingyuanyun\BaseResponse;

/**
 * 查询模板-响应类
 *
 * @method array   getTemplateList()     获取返回结果的data值
 */
class TemplateQueryResponse extends BaseResponse
{
    /**
     * 结果数据，存储套打模板的数组
     * 格式：[['templateId' => '123','templateName' => '第一个模板'],['templateId' => '456','templateName' => '第二个模板']]
     * @var array
     */
    public $templateList = [];

    /**
     * 响应结果解析
     */
    protected function unmarshal()
    {
        $this->errcode = $this->response->getBody('code', self::ERRCODE_OK);

        if ($this->isSuccess() && $this->errcode == self::ERRCODE_OK) {
            $this->templateList = $this->response->getBody('data');
        } else {
            $this->errmsg = $this->response->getBody('msg', '');
        }
    }
}
