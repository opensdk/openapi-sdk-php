<?php

namespace Mingyuanyun\Printing\V20230207;

use Mingyuanyun\BaseRequest;
use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Printing\Validator\TemplateQueryValidator;


/**
 * 查询模板-请求数据类
 *  @method string getBizSceneCode() return field $bizSceneCode value
 */
class TemplateQueryRequest extends BaseRequest
{
    use AccessTrait;
    /**
     * 场景code
     * @var string
     */
    private $bizSceneCode;

    /**
     * @param $bizSceneCode
     * @return $this
     */
    public function setBizSceneCode($bizSceneCode)
    {
        TemplateQueryValidator::bizSceneCode($bizSceneCode);
        $this->bizSceneCode = $bizSceneCode;
        return $this;
    }
}
