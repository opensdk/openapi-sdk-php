<?php

namespace Mingyuanyun\Printing\Validator;

use Mingyuanyun\Core\Exception\InvalidArgumentException;

/**
 * 套打查询模板校验器
 */
class TemplateQueryValidator
{
    /**
     * @param string $bizSceneCode
     * @throws InvalidArgumentException
     */
    public static function bizSceneCode($bizSceneCode)
    {
        if (!is_string($bizSceneCode)) {
            throw new InvalidArgumentException('$bizSceneCode must be a string.');
        }
        if (empty($bizSceneCode)) {
            throw new InvalidArgumentException('$bizSceneCode cannot be empty.');
        }
    }
}
