<?php

namespace Mingyuanyun\Token\V20230109;

use Mingyuanyun\BaseResponse;

/**
 * 访问令牌服务响应类
 *
 * @method string   getAccessToken()    获取访问令牌
 * @method int      getExpireTime()     获取令牌有效时长
 */
class AccessTokenResponse extends BaseResponse
{
    /**
     * 访问令牌
     *
     * @var string
     */
    public $accessToken = '';

    /**
     * 令牌有效时长，单位：秒
     *
     * @var int
     */
    public $expireTime = 0;

    /**
     * 解析响应数据
     */
    protected function unmarshal()
    {
        if (!$this->isSuccess()) {
            return $this->errmsg = $this->response->toString();
        }

        $this->accessToken = $this->response->getBody('access_token', '');
        $this->expireTime  = $this->response->getBody('expires_in', 0);
    }

    /**
     * @return bool
     */
    public function isSuccess($soEasy = false, $okCode = self::ERRCODE_OK)
    {
        if (!$soEasy) {
            return parent::isSuccess($soEasy, $okCode);
        }

        // $soEasy = true

        if (!parent::isSuccess()) {
            return false;
        }

        // 如果有 Result 参数返回，且值为 false，可认为接口请求失败
        $result = $this->response->getBody('Result');
        return !is_null($result) && $result === false;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return sprintf("accessToken: (%d) %s", $this->expireTime, $this->accessToken ?: 'null');
    }
}
