<?php

namespace Mingyuanyun\Token\V20230109;

use Mingyuanyun\BaseClient;
use Mingyuanyun\Core\Credentials\ClientCredentials;
use Mingyuanyun\Core\Support\RequestOptions;

/**
 * 访问令牌服务能力客户端
 * 可用于获取开放服务访问所需要的访问令牌数据
 *
 * @method static $this prepare(ClientCredentials $credentials, AccessTokenRequest $request)
 */
class AccessTokenClient extends BaseClient
{
    /**
     * @var ClientCredentials
     */
    protected $credentials;

    /**
     * 访问令牌服务版本
     *
     * @var string
     */
    protected $version = '2023-01-09';

    /**
     * 访问令牌的获取无需依赖令牌的获取
     *
     * @var bool
     */
    protected $isRequiredAccessToken = false;

    /**
     * @return string
     */
    protected function getUri()
    {
        return '/oauth/accessToken';
    }

    /**
     * @return string
     */
    protected function getMethod()
    {
        return RequestOptions::METHOD_POST;
    }

    /**
     * 访问令牌服务构造器
     *
     * @param ClientCredentials     $credentials    使用开放服务所需要的凭证数据
     * @param AccessTokenRequest    $request        请求服务所提供的请求数据
     */
    public function __construct(ClientCredentials $credentials, AccessTokenRequest $request)
    {
        parent::__construct($credentials, $request);
    }

    /**
     * @return void
     */
    protected function resolveRequestValues()
    {
        $body = [
            'client_id'     => $this->credentials->getSecretId(),
            'client_secret' => $this->credentials->getSecretKey(),
        ];
        $this->request->setBody($body);
        $this->request->setFormat(RequestOptions::FORMAT_JSON);
    }

    /**
     * @return AccessTokenResponse
     * @throws ResponseException
     */
    public function invoke()
    {
        return new AccessTokenResponse(parent::invoke());
    }
}
