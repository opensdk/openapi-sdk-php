<?php

namespace Mingyuanyun\Token\V20230109;

use Mingyuanyun\BaseRequest;

/**
 * 访问令牌服务请求数据类
 * 
 * @codeCoverageIgnore
 */
class AccessTokenRequest extends BaseRequest
{

}
