<?php

namespace Mingyuanyun;

use Mingyuanyun\Core\Request;
use Mingyuanyun\Core\RequestInterface;
use Mingyuanyun\Core\Support\Traits\AccessTrait;

/**
 * 客户端请求基类
 * 
 * @codeCoverageIgnore
 */
abstract class BaseRequest implements RequestInterface
{
    use AccessTrait;
    
    /**
     * @var Request
     */
    protected $request;

    /**
     * 客户端请求构造器
     */
    public function __construct()
    {
        $this->request = new Request();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->request->getId();
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->request->getUri();
    }

    /**
     * @param string $uri
     * @return static
     */
    public function setUri($uri)
    {
        $this->request->setUri($uri);
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->request->getMethod();
    }

    /**
     * @param string $method
     * @return static
     */
    public function setMethod($method)
    {
        $this->request->setMethod($method);
        return $this;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->request->getQuery();
    }

    /**
     * @param array $query
     * @return static
     */
    public function setQuery(array $query)
    {
        $this->request->setQuery($query);
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->request->getHeaders();
    }

    /**
     * @param array $headers
     * @return static
     */
    public function setHeaders(array $headers)
    {
        $this->request->setHeaders($headers);
        return $this;
    }

    /**
     * @return array
     */
    public function getBody()
    {
        return $this->request->getBody();
    }

    /**
     * @param array $body
     * @return static
     */
    public function setBody(array $body)
    {
        $this->request->setBody($body);
        return $this;
    }

    /**
     * @return array
     */
    public function getCookies()
    {
        return $this->request->getCookies();
    }

    /**
     * @param array $cookies
     * @return static
     */
    public function setCookies(array $cookies)
    {
        $this->request->setCookies($cookies);
        return $this;
    }

    /**
     * @return bool
     */
    public function isJsonFormat()
    {
        return $this->request->isJsonFormat();
    }

    /**
     * @return bool
     */
    public function isFormFormat()
    {
        return $this->request->isFormFormat();
    }

    /**
     * @param string $format
     * @return static
     */
    public function setFormat($format)
    {
        $this->request->setFormat($format);
        return $this;
    }

    /**
     * @return float
     */
    public function getConnectTimeout()
    {
        return $this->request->getConnectTimeout();
    }

    /**
     * @return float
     */
    public function getTimeout()
    {
        return $this->request->getTimeout();
    }

    /**
     * @return bool
     */
    public function getVerify()
    {
        return $this->request->getVerify();
    }

    /**
     * @return int[]
     */
    public function getClientRetryCodes()
    {
        return $this->request->getClientRetryCodes();
    }

    /**
     * @return string[]
     */
    public function getClientRetryStrings()
    {
        return $this->request->getClientRetryStrings();
    }

    /**
     * @return int[]
     */
    public function getServerRetryCodes()
    {
        return $this->request->getServerRetryCodes();
    }

    /**
     * @return string[]
     */
    public function getServerRetryStrings()
    {
        return $this->request->getServerRetryStrings();
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->request->toString();
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->request->getStatus();
    }

    public function pending()
    {
        $this->request->pending();
    }

    public function sending()
    {
        $this->request->sending();
    }

    public function complete()
    {
        $this->request->complete();
    }

    public function reset()
    {
        $this->request->reset();
    }
}
