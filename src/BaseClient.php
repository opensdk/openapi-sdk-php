<?php

namespace Mingyuanyun;

use Mingyuanyun\Core\Client;
use Mingyuanyun\Core\ClientInterface;
use Mingyuanyun\Core\CredentialsInterface;
use Mingyuanyun\Core\Credentials\ClientCredentials;
use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\Response;
use Mingyuanyun\Core\Support\Helper\StringHelper;
use Mingyuanyun\Token\V20230109\AccessTokenClient;
use Mingyuanyun\Token\V20230109\AccessTokenRequest;

/**
 * 客户端基类
 */
abstract class BaseClient implements ClientInterface
{
    /**
     * @var CredentialsInterface
     */
    protected $credentials;

    /**
     * 服务接口的版本号
     * 示例：2023-01-06
     *
     * @var string
     */
    protected $version;

    /**
     * 客户端请求数据类
     *
     * @var BaseRequest
     */
    protected $request;

    /**
     * 标准客户端请求类
     *
     * @var Client
     */
    protected $client;

    /**
     * 客户端请求是否需要访问令牌，默认 true
     *
     * @var bool
     */
    protected $isRequiredAccessToken = true;

    /**
     * 客户端构造器
     *
     * @param CredentialsInterface  $credentials    使用开放服务所需要的凭证数据
     * @param BaseRequest           $request        请求服务所提供的请求数据
     */
    public function __construct(CredentialsInterface $credentials, BaseRequest $request)
    {
        $this->credentials = $credentials;
        $this->request     = $request;
    }

    /**
     * 快速准备好一个客户端实例
     *
     * @param CredentialsInterface  $credentials    使用开放服务所需要的凭证数据
     * @param BaseRequest           $request        请求服务所提供的请求数据
     * @return static
     */
    public static function prepare(CredentialsInterface $credentials, BaseRequest $request)
    {
        return new static($credentials, $request);
    }

    /**
     * 服务接口请求路径
     *
     * @return string
     */
    abstract protected function getUri();

    /**
     * 服务接口请求方式
     *
     * @return string
     */
    abstract protected function getMethod();

    /**
     * 往请求数据类中追加设置不同服务接口所需的 Query、Headers、Body 等数据内容
     */
    abstract protected function resolveRequestValues();

    /**
     * 获取服务请求的前置处理
     */
    protected function beforeInvoke()
    {
        $this->request->setUri($this->getUri());
        $this->request->setMethod($this->getMethod());
        if (!empty($this->version)) {
            $this->request->setHeaders([StringHelper::hk('client-version') => $this->version]);
        }

        $this->client = new Client($this->request->getRequest());
    }

    /**
     * 处理接口请求所需要的访问凭证
     */
    protected function resolveAccessToken()
    {
        if (!$this->isRequiredAccessToken) {
            return;
        }
        if (!$this->credentials instanceof ClientCredentials) {
            return;
        }

        $token = $this->credentials->getToken();
        if (empty($token)) {
            $token = $this->getAccessToken($this->credentials);
        }

        $this->request->setHeaders(['Authorization' => 'Bearer ' . $token]);
    }

    /**
     * 获取鉴权接口鉴权凭证
     *
     * @param ClientCredentials $credentials
     * @return string
     * @throws RequestException
     */
    protected function getAccessToken(ClientCredentials $credentials)
    {
        $response = AccessTokenClient::prepare($credentials, new AccessTokenRequest())->invoke();
        if (empty($response->getAccessToken())) {
            throw new RequestException('Prepare [accessToken] faild! ' . $response->getErrMsg());
        }

        return $response->getAccessToken();
    }

    /**
     * 获取服务请求的响应数据
     *
     * @return Response
     */
    public function invoke()
    {
        $this->resolveRequestValues();

        $this->resolveAccessToken();

        $this->beforeInvoke();

        return $this->client->invoke();
    }
}
