<?php

namespace Mingyuanyun;

use Composer\Script\Event;

/**
 * Composer Script: release
 *
 * @codeCoverageIgnore
 */
class Release
{
    public static function release(Event $event)
    {
        $args = $event->getArguments();
        if (count($args) <= 1) {
            echo 'Missing change log.';
            return;
        }

        self::updateChangelogFile($args[0], $args[1]);
    }

    /**
     * @param $version
     * @param $changeLog
     */
    private static function updateChangelogFile($version, $changeLog)
    {
        $content = preg_replace(
            '/# CHANGELOG/',
            '# CHANGELOG'
            . "\n"
            . "\n"
            . "## $version - " . date('Y-m-d')
            . self::log($changeLog),
            self::getChangeLogContent()
        );

        file_put_contents(self::getChangeLogFile(), $content);
    }

    /**
     * @param $changeLog
     *
     * @return string
     */
    private static function log($changeLog)
    {
        $logs   = explode('|', $changeLog);
        $string = "\n";
        foreach ($logs as $log) {
            if ($log) {
                $string .= "- $log." . "\n";
            }
        }

        return $string;
    }

    /**
     * @return string
     */
    private static function getChangeLogContent()
    {
        return file_get_contents(self::getChangeLogFile());
    }

    /**
     * @return string
     */
    private static function getChangeLogFile()
    {
        return __DIR__ . '/../CHANGELOG.md';
    }
}
