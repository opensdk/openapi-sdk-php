<?php

namespace Mingyuanyun\Message\V20230417;

use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Message\V20230417\Support\BaseMessage;
use Mingyuanyun\Message\V20230417\Support\SceneMessage;

/**
 * 场景消息推送请求数据类
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 *
 * @method string getSceneCode() 获取场景编码
 * @method SceneMessage getMessage() 获取场景消息的消息内容对象
 */
class ScenePushRequest extends PushRequest
{
    use AccessTrait;

    /**
     * 在消息服务中设置的场景编码
     *
     * @var string
     */
    private $sceneCode;

    /**
     * 设置场景编码
     *
     * @param string $sceneCode 场景编码
     * @return $this
     */
    public function setSceneCode($sceneCode)
    {
        PushValidator::sceneCode($sceneCode);

        $this->sceneCode = $sceneCode;
        return $this;
    }

    /**
     * 设置场景消息内容
     *
     * @param SceneMessage $message 场景消息内容对象
     * @return $this
     */
    public function setMessage(BaseMessage $message)
    {
        if (!$message instanceof SceneMessage) {
            throw new RequestException('message is must be instance of SceneMessage at scene push.');
        }

        $this->message = $message;
        return $this;
    }
}
