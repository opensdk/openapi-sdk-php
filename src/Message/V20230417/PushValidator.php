<?php

namespace Mingyuanyun\Message\V20230417;

use InvalidArgumentException;
use Mingyuanyun\Message\V20230417\Support\ChannelType;
use Mingyuanyun\Message\V20230417\Support\Receiver;

/**
 * 消息推送数据校验器
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月05日
 */
class PushValidator
{
    /**
     * 消息推送来源校验
     *
     * @throws InvalidArgumentException
     */
    public static function source($source)
    {
        if (empty($source)) {
            throw new InvalidArgumentException('$source cannot be empty.');
        }
        if (!is_string($source)) {
            throw new InvalidArgumentException('$source must be a string.');
        }
    }

    /**
     * 租户号校验
     *
     * @throws InvalidArgumentException
     */
    public static function tenantCode($tenantCode)
    {
        if (empty($tenantCode)) {
            throw new InvalidArgumentException('$tenantCode cannot be empty.');
        }
        if (!is_string($tenantCode) || !preg_match('/^[a-z]+$/', $tenantCode)) {
            throw new InvalidArgumentException('$tenantCode must be a string and consist of [a-z].');
        }
    }

    /**
     * 渠道类型校验
     *
     * @throws InvalidArgumentException
     */
    public static function channelType($channelType)
    {
        if (empty($channelType)) {
            throw new InvalidArgumentException('$channelType cannot be empty.');
        }
        if (!is_string($channelType)) {
            throw new InvalidArgumentException('$channelType must be a string.');
        }
        if (!in_array($channelType, ChannelType::toArray())) {
            throw new InvalidArgumentException(sprintf('invalid $channelType, it must be in [%s]', ChannelType::toString()));
        }
    }

    /**
     * 场景编码校验
     *
     * @throws InvalidArgumentException
     */
    public static function sceneCode($sceneCode)
    {
        if (empty($sceneCode)) {
            throw new InvalidArgumentException('$sceneCode cannot be empty.');
        }
        if (!is_string($sceneCode) || !preg_match('/^[a-zA-Z_\-\.]+$/', $sceneCode)) {
            throw new InvalidArgumentException('$sceneCode must be a string and consist of [a-zA-Z_\-\.].');
        }
    }

    /**
     * 消息标题校验
     *
     * @throws InvalidArgumentException
     */
    public static function messageTitle($title)
    {
        if (empty($title)) {
            throw new InvalidArgumentException('$title cannot be empty.');
        }
        if (!is_string($title)) {
            throw new InvalidArgumentException('$title must be a string.');
        }
        // 中文编码下字符长度不允许超出 100
        if (mb_strlen($title, 'UTF-8') > 100) {
            throw new InvalidArgumentException('$title character length should not exceed 100.');
        }
    }

    /**
     * 消息内容详情校验
     *
     * @throws InvalidArgumentException
     */
    public static function messageContent($content)
    {
        if (empty($content)) {
            throw new InvalidArgumentException('$content cannot be empty.');
        }
        if (!is_string($content)) {
            throw new InvalidArgumentException('$content must be a string.');
        }
        // 中文编码下字符长度不允许超出 1000
        if (mb_strlen($content, 'UTF-8') > 1000) {
            throw new InvalidArgumentException('$content character length should not exceed 1000.');
        }
    }

    /**
     * 消息内容跳转地址校验
     *
     * @throws InvalidArgumentException
     */
    public static function messageRedirectUrl($redirectUrl)
    {
        if (empty($redirectUrl)) {
            return;
        }
        if (!is_string($redirectUrl)) {
            throw new InvalidArgumentException('$redirectUrl must be a string.');
        }
    }

    /**
     * 接收人列表校验
     *
     * @throws InvalidArgumentException
     */
    public static function receivers(array $receivers)
    {
        if (empty($receivers)) {
            throw new InvalidArgumentException('$receivers cannot be empty.');
        }
        // 单次最多只允许给 100 个接收人推消息
        if (count($receivers) > 100) {
            throw new InvalidArgumentException('$receivers size should not exceed 100.');
        }

        foreach ($receivers as $receiver) {
            static::receiver($receiver);
        }
    }

    /**
     * 接收人校验
     *
     * @throws InvalidArgumentException
     */
    public static function receiver(Receiver $receiver)
    {
        if (empty($receiver->getContact())) {
            throw new InvalidArgumentException('$contact of receiver cannot be empty.');
        }
        // 接收人的联系方式长度不允许超过 100 个字符
        if (strlen($receiver->getContact()) > 100) {
            throw new InvalidArgumentException('$contact of receiver character length should not exceed 100.');
        }
    }
}
