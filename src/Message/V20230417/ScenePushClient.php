<?php

namespace Mingyuanyun\Message\V20230417;

use Mingyuanyun\BaseClient;
use Mingyuanyun\Core\CredentialsInterface;
use Mingyuanyun\Core\Exception\ClientException;
use Mingyuanyun\Core\Support\RequestOptions;

/**
 * 场景消息推送客户端
 * 用于发起一个场景消息的推送
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月05日
 */
class ScenePushClient extends BaseClient
{
    /**
     * 场景消息推送版本号
     * 
     * @var string
     */
    protected $version = '2023-04-17';
    
    /**
     * 请求数据对象
     *
     * @var ScenePushRequest
     */
    protected $request;

    /**
     * 渠道消息推送客户端构造方法
     */
    public function __construct(CredentialsInterface $credentials, ScenePushRequest $request)
    {
        if (empty($request->getSceneCode()) || empty($request->getSource())) {
            throw new ClientException('init client failded, basic data sceneCode and source is requried.');
        }
        if (empty($request->getMessage())) {
            throw new ClientException('init client failded, message is required.');
        }
        if (empty($request->getReceivers())) {
            throw new ClientException('init client failded, receivers is empty.');
        }
        parent::__construct($credentials, $request);
    }

    /**
     * 接口请求方式
     *
     * @return string
     */
    protected function getMethod()
    {
        return RequestOptions::METHOD_POST;
    }

    /**
     * 接口路径
     *
     * @return string
     */
    protected function getUri()
    {
        return '/message/v1/message/scene-push';
    }

    protected function resolveRequestValues()
    {
        $data = [];

        $this->resolveBaseValues($data);
        $this->resolveMessageValues($data);
        $this->resolveReceiverValues($data);

        $this->request->setBody($data);
        $this->request->setFormat(RequestOptions::FORMAT_JSON);
    }

    /**
     * 发起消息推送
     *
     * @return PushResponse
     */
    public function invoke()
    {
        return new PushResponse(parent::invoke());
    }

    /**
     * 解决推送基础数据的准备工作
     *
     * @param &$data 请求数据数组
     */
    private function resolveBaseValues(&$data)
    {
        $data['sceneCode']  = $this->request->getSceneCode();
        $data['source']     = $this->request->getSource();
        $data['tenantCode'] = $this->request->getTenantCode();
    }

    /**
     * 解决消息内容相关的数据准备工作
     *
     * @param &$data 请求数据数组
     */
    private function resolveMessageValues(&$data)
    {
        $data['message'] = $this->request->getMessage()->toArray();
    }

    /**
     * 解决接收人相关的数据准备工作
     *
     * @param &$data 请求数据数组
     */
    private function resolveReceiverValues(&$data)
    {
        $receivers = $this->request->getReceivers();

        foreach ($receivers as $receiver) {
            $value = ['contact' => $receiver->getContact()];

            $data['receivers'][] = $value;
        }
    }
}
