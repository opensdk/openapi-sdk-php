<?php

namespace Mingyuanyun\Message\V20230417\Support;

/**
 * 场景消息
 * 
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月19日
 */
class SceneMessage extends BaseMessage
{
    /**
     * 类型：场景
     * 专门用于场景消息推送使用的消息结构体
     * 
     * @var string
     */
    protected $type = 'scene';

    /**
     * 自定义数据体
     * 
     * @var array
     */
    private $params = [];

    /**
     * 构造方法
     * 
     * @param array $params 自定义数据体
     */
    public function __construct(array $params)
    {
        if (!is_null($params)) {
            $this->params = $params;
        }
    }

    public function toArray()
    {
        return ['params' => $this->params];
    }
}