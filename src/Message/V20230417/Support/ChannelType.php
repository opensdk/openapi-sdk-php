<?php

namespace Mingyuanyun\Message\V20230417\Support;

/**
 * 支持的消息渠道类型
 * 
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 */
class ChannelType
{
    /** APP */
    const APP = 'APP';
    /** 微信公众号 */
    const WX_OFFICIAL_ACCOUNT = 'WX_OFFICIAL_ACCOUNT';
    /** 第三方系统（系统站内信） */
    const THIRD_SYSTEM = 'THIRD_SYSTEM';

    public static function toArray()
    {
        return [self::APP, self::WX_OFFICIAL_ACCOUNT, self::THIRD_SYSTEM];
    }

    public static function toString()
    {
        return implode('|', self::toArray());
    }
}