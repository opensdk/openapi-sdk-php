<?php

namespace Mingyuanyun\Message\V20230417\Support;

use Mingyuanyun\Core\Support\Traits\AccessTrait;

/**
 * 接收人数据结构
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 *
 * @method string getContactProvider() 获取联系方式提供方
 * @method string getContact() 获取联系方式
 * @method string getExtra() 获取额外的补充数据
 */
class Receiver
{
    use AccessTrait;

    /**
     * 联系方式提供方
     *
     * @var string
     */
    private $contactProvider;

    /**
     * 联系方式
     *
     * @var string
     */
    private $contact;

    /**
     * 额外的补充数据
     *
     * @var string|null
     */
    private $extra;

    /**
     * 接收人构造方法
     *
     * @param string        $contact            联系方式
     * @param string|null   $contactProvider    联系方式提供方
     * @param string|null   $extra              额外的补充数据
     */
    public function __construct($contact, $contactProvider = null, $extra = null)
    {
        $this->contact = $contact;

        if (!is_null($contactProvider)) {
            $this->contactProvider = $contactProvider;
        }
        if (!is_null($extra)) {
            $this->extra = $extra;
        }
    }
}
