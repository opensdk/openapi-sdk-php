<?php

namespace Mingyuanyun\Message\V20230417\Support;

/**
 * 文本消息
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 */
class TextMessage extends BaseMessage
{
    /**
     * 类型：文本
     *
     * @var string
     */
    protected $type = 'text';

    /**
     * 消息点击后的跳转外链地址
     *
     * @var string
     */
    private $redirectUrl;

    /**
     * 构造方法
     *
     * @param string $title 标题
     * @param string $content 内容详情
     * @param string|null $redirectUrl 消息点击后跳转外链地址
     */
    public function __construct($title, $content, $redirectUrl = null)
    {
        parent::__construct($title, $content);

        $this->redirectUrl = $redirectUrl;
    }

    public function toArray()
    {
        $result = [
            'type'    => $this->type,
            'title'   => $this->title,
            'content' => $this->content,
        ];
        if (!is_null($this->redirectUrl)) {
            $result['redirectUrl'] = $this->redirectUrl;
        }
        return $result;
    }
}
