<?php

namespace Mingyuanyun\Message\V20230417\Support;

use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Message\V20230417\PushValidator;

/**
 * 消息基础结构
 * 
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 * 
 * @method string getType() 获取消息类型
 * @method string getTitle() 获取消息标题
 * @method string getContent() 获取消息内容
 */
abstract class BaseMessage
{
    use AccessTrait;

    /**
     * 类型
     * 
     * @var string
     */
    protected $type;

    /**
     * 标题
     * 
     * @var string
     */
    protected $title;

    /**
     * 内容详情
     * 
     * @var string
     */
    protected $content;

    /**
     * 抽象父类构造方法
     * 
     * @param string $title 标题
     * @param string $content 内容详情
     */
    protected function __construct($title, $content)
    {
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * 将消息数据转换成对应的数组结构，用于后续的接口调用数据传输
     * 
     * @return array
     */
    public abstract function toArray();
}