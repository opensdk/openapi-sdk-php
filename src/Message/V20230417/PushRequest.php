<?php

namespace Mingyuanyun\Message\V20230417;

use Mingyuanyun\BaseRequest;
use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Message\V20230417\Support\BaseMessage;
use Mingyuanyun\Message\V20230417\Support\Receiver;

/**
 * 消息推送请求数据基类
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 *
 * @method string getSource()       获取调用方来源标识
 * @method string getTenantCode()   获取租户号
 * @method BaseMessage getMessage() 获取消息内容对象
 * @method Receiver[] getReceivers() 获取接收人对象列表
 */
class PushRequest extends BaseRequest
{
    use AccessTrait;

    /**
     * 推送来源（发起本次推送的调用方标识）
     *
     * @var string
     */
    protected $source;

    /**
     * 租户号
     *
     * @var string
     */
    protected $tenantCode;

    /**
     * 消息内容
     *
     * @var BaseMessage
     */
    protected $message;

    /**
     * 接收人列表
     * @var Receiver[]
     */
    protected $receivers;

    /**
     * 设置调用来源
     *
     * @param string $source 来源标识
     * @return $this
     */
    public function setSource($source)
    {
        PushValidator::source($source);

        $this->source = $source;
        return $this;
    }

    /**
     * 设置租户号
     *
     * @param string $tenantCode 租户号
     * @return $this
     */
    public function setTenantCode($tenantCode)
    {
        PushValidator::tenantCode($tenantCode);

        $this->tenantCode = $tenantCode;
        return $this;
    }

    /**
     * 设置消息内容
     *
     * @param BaseMessage $message 消息内容对象
     * @return $this
     */
    public function setMessage(BaseMessage $message)
    {
        PushValidator::messageTitle($message->getTitle());
        PushValidator::messageContent($message->getContent());

        $this->message = $message;
        return $this;
    }

    /**
     * 设置接收人数据列表
     *
     * @param Receiver[] 接收人对象列表
     * @return $this
     */
    public function setReceivers(array $receivers)
    {
        PushValidator::receivers($receivers);

        $this->receivers = $receivers;
        return $this;
    }
}
