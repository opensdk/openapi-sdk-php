<?php

namespace Mingyuanyun\Message\V20230417;

use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Message\V20230417\Support\TextMessage;

/**
 * 渠道消息推送请求数据类
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月04日
 *
 * @method string getChannelType() 获取渠道类型
 */
class ChannelPushRequest extends PushRequest
{
    use AccessTrait;

    /**
     * 待推送的渠道类型。
     * 1. APP - APP 通知渠道
     * 2. WX_OFFICIAL_ACCOUNT - 微信公众号渠道
     *
     * @var string
     */
    private $channelType;

    /**
     * 设置渠道类型
     *
     * @param string $channelType 渠道类型
     * @return $this
     */
    public function setChannelType($channelType)
    {
        PushValidator::channelType($channelType);

        $this->channelType = $channelType;
        return $this;
    }

    /**
     * 设置文本消息的内容数据
     *
     * @param string $title 标题
     * @param string $content 内容详情
     * @param string|null $redirectUrl 消息点击后的跳转地址
     * @return $this
     */
    public function setMessageText($title, $content, $redirectUrl = null)
    {
        PushValidator::messageTitle($title);
        PushValidator::messageContent($content);
        PushValidator::messageRedirectUrl($redirectUrl);

        $this->message = new TextMessage($title, $content, $redirectUrl);
        return $this;
    }
}
