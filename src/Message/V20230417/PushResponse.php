<?php

namespace Mingyuanyun\Message\V20230417;

use Mingyuanyun\BaseResponse;
use Mingyuanyun\Core\Support\Traits\AccessTrait;

/**
 * 消息推送相应数据类
 * 渠道消息推送&场景消息推送都共用一个返回数据结构
 *
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @date 2023年05月05日
 *
 * @method int getTaskId() 获取推送成功后生成的消息任务 ID
 */
class PushResponse extends BaseResponse
{
    use AccessTrait;

    /**
     * @var int
     */
    private $taskId;

    protected function unmarshal()
    {
        $this->errcode = $this->response->getBody('code', self::ERRCODE_OK);

        if (!$this->isSuccess(true)) {
            $this->errmsg = $this->response->getBody('message', $this->response->toString());
            return;
        }

        $this->taskId = $this->response->getBody('data.taskId');
    }

    public function isSuccess($soEasy = false, $okCode = self::ERRCODE_OK)
    {
        if (!$soEasy) {
            return parent::isSuccess(false, $okCode);
        }

        // $soEasy = true

        if (!parent::isSuccess(false, $okCode)) {
            return false;
        }

        $errcode = $this->response->getBody('code', 10000000);
        return $errcode === $okCode;
    }
}
